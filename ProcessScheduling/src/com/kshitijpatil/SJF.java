package com.kshitijpatil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class SJF {
	List<Process> prList;
	int totalTime;

	public SJF(List<Process> prList) {
		this.prList = prList;
		Collections.sort(prList, (Process p1, Process p2) -> {
			// Sort by arrival time, then burst time
			int c1 = p1.at - p2.at;
			return (c1 == 0) ? p1.bt - p2.bt : c1;
		});
	};
	public List<Process> getSchedule() {
		// tt = min(at) + sum(bt);
		int sum = prList.stream().mapToInt(p -> p.bt).sum();
		int totalTime = prList.get(0).at + sum;
		
		PriorityQueue<Process> waitingQueue = new PriorityQueue<Process>((p1,p2) -> p1.bt - p2.bt);
		
		int i = 0;
		List<Process> schedulingList = prList;
		List<Process> schedule = new ArrayList<Process>();
		Process currProcess = prList.get(0);
		while (i < totalTime) {
			//System.out.println(currProcess.pid + "\t");
			schedulingList.remove(currProcess);
			i += currProcess.bt;
			currProcess.ct = i;
			currProcess.tat = i - currProcess.at;
			currProcess.wt = currProcess.tat - currProcess.bt;
			schedule.add(currProcess);
			
			for (int j =0; j < schedulingList.size(); j++) {
				if (schedulingList.get(j).at <= i && schedulingList.get(j).pid != currProcess.pid) {
					waitingQueue.add(schedulingList.get(j));
					schedulingList.remove(j);
				}
			}
			currProcess = waitingQueue.poll();
		}
		return schedule;
	}
	
	public List<Process> getList() {
		return prList;
	}
}
