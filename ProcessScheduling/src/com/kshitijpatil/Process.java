package com.kshitijpatil;

public class Process {
	public int pid;
	public int bt;
	public int at;
	public int wt;
	public int tat;
	public int ct;
	
	public Process(int pid, int at, int bt) {
		this.pid = pid;
		this.at = at;
		this.bt = bt;
	}
	
	@Override
	public String toString() {
		return "P" + pid + "\t" +
				at + "\t" +
				bt + "\t" + 
				ct + "\t" +
				tat + "\t" +
				wt;
	}
	
	
}
