package com.kshitijpatil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Scheduling {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter No. of Processes: ");
		int pnum = in.nextInt();
		List<Process> prList = new ArrayList<Process>();
		int at, bt;
		for (int i = 0; i < pnum; i++) {
			System.out.print("Enter arrival & burst time for Process " + (i+1) + ": ");
			at = in.nextInt();
			bt = in.nextInt();
			prList.add(new Process(i+1, at, bt));
		}
		SJF sjf = new SJF(prList);
		
		System.out.println("PID\tAT\tBT\tCT\tTAT\tWT\n");
		sjf.getSchedule().forEach(System.out::println);
	}

}
